# Bus Stop

Bus Stop is a preset for the Moodle activity database.

## Demo

https://fdagner.de/moodle/mod/data/view.php?d=10

## Getting started

Download the source code and zip the files WITHOUT parent folder. Create a "Database" activity in Moodle and then upload the ZIP file in the "Presets" tab under "Import".

## Language Support

The preset is available in German. 

## Description

The "bus stop" method helps students find partners and allows them to learn at their own pace. 

## License

https://creativecommons.org/licenses/by/4.0/

## Screenshots

<img width="400" alt="single view" src="/screenshots/listenansicht.png">
